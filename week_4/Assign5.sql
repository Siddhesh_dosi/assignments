use company;

IF OBJECT_ID('SALES','U') IS NOT NULL
DROP TABLE  SALES;

CREATE TABLE SALES
(
       SALE_ID        INTEGER,
       PRODUCT_ID     INTEGER,
       YEAR           INTEGER,
       Quantity       INTEGER,
       PRICE          INTEGER
);

BEGIN TRAN
INSERT INTO SALES VALUES ( 1, 100, 2008, 10, 5000);
INSERT INTO SALES VALUES ( 2, 100, 2009, 12, 5000);
INSERT INTO SALES VALUES ( 3, 100, 2010, 25, 5000);
INSERT INTO SALES VALUES ( 4, 100, 2011, 16, 5000);
INSERT INTO SALES VALUES ( 5, 100, 2012, 8,  5000);

INSERT INTO SALES VALUES ( 6, 200, 2010, 10, 9000);
INSERT INTO SALES VALUES ( 7, 200, 2011, 15, 9000);
INSERT INTO SALES VALUES ( 8, 200, 2012, 20, 9000);
INSERT INTO SALES VALUES ( 9, 200, 2008, 13, 9000);
INSERT INTO SALES VALUES ( 10,200, 2009, 14, 9000);

INSERT INTO SALES VALUES ( 11, 300, 2010, 20, 7000);
INSERT INTO SALES VALUES ( 12, 300, 2011, 18, 7000);
INSERT INTO SALES VALUES ( 13, 300, 2012, 20, 7000);
INSERT INTO SALES VALUES ( 14, 300, 2008, 17, 7000);
INSERT INTO SALES VALUES ( 15, 300, 2009, 19, 7000);
COMMIT;

SELECT * FROM SALES;
/* ====================================== output =======================================

SALE_ID     PRODUCT_ID  YEAR        Quantity    PRICE
----------- ----------- ----------- ----------- -----------
1           100         2008        10          5000
2           100         2009        12          5000
3           100         2010        25          5000
4           100         2011        16          5000
5           100         2012        8           5000
6           200         2010        10          9000
7           200         2011        15          9000
8           200         2012        20          9000
9           200         2008        13          9000
10          200         2009        14          9000
11          300         2010        20          7000
12          300         2011        18          7000
13          300         2012        20          7000
14          300         2008        17          7000
15          300         2009        19          7000
=================================================================================*/

------------------------------Ques 1 ---------------
---- Aggregate function
Select YEAR, count(PRODUCT_ID) as Total_sales
from SALES
Group by Year;
Go

-- windows function
Select SALE_ID, PRODUCT_ID, YEAR, Quantity, PRICE, 
count(PRODUCT_ID) over (partition by YEAR) as CNT
From SALES;
Go

/* ====================================== output =======================================


YEAR        Total_sales
----------- -----------
2008        3
2009        3
2010        3
2011        3
2012        3


SALE_ID     PRODUCT_ID  YEAR        Quantity    PRICE       CNT
----------- ----------- ----------- ----------- ----------- -----------
1           100         2008        10          5000        3
9           200         2008        13          9000        3
14          300         2008        17          7000        3
15          300         2009        19          7000        3
10          200         2009        14          9000        3
2           100         2009        12          5000        3
3           100         2010        25          5000        3
11          300         2010        20          7000        3
6           200         2010        10          9000        3
7           200         2011        15          9000        3
12          300         2011        18          7000        3
4           100         2011        16          5000        3
5           100         2012        8           5000        3
13          300         2012        20          7000        3
8           200         2012        20          9000        3
=========================================================================================*/

/*------------------------Ques 2-----------------------
2. Write a SQL query using the analytic function to find the total sales(QUANTITY) of each product?
----------------------------------------------------------*/

Select PRODUCT_ID, Quantity, 
SUM(Quantity) over (partition by PRODUCT_ID) as TOT_SALES
From SALES;
Go
/* ====================================== output =======================================

PRODUCT_ID  Quantity    TOT_SALES
----------- ----------- -----------
100         10          71
100         12          71
100         25          71
100         16          71
100         8           71
200         10          72
200         15          72
200         20          72
200         13          72
200         14          72
300         20          94
300         18          94
300         20          94
300         17          94
300         19          94
====================================================================*/


/*------------------------------------------
3. Write a SQL query to find the cumulative sum of sales(QUANTITY) of each product? Here first sort the QUANTITY in ascendaing order for each product and then accumulate the QUANTITY.
Cumulative sum of QUANTITY for a product = QUANTITY of current row + sum of QUANTITIES all previous rows in that product.

-------------------------------------------------------*/

Select PRODUCT_ID, Quantity, Sum(Quantity) 
             Over ( Partition By PRODUCT_ID  Order By Quantity ) As CUM_SALES
From SALES;
Go
/* ====================================== output =======================================
PRODUCT_ID  Quantity    CUM_SALES
----------- ----------- -----------
100         8           8
100         10          18
100         12          30
100         16          46
100         25          71
200         10          10
200         13          23
200         14          37
200         15          52
200         20          72
300         17          17
300         18          35
300         19          54
300         20          94
300         20          94
======================================================================*/

/*---------------------------------------------

4. Write a SQL query to find the sum of sales of current row and previous 2 rows in a product group?
Sort the data on sales and then find the sum.

--------------------------------------------------*/

SELECT PRODUCT_ID, Quantity,
LAG(Quantity,0,0) over (PARTITION by PRODUCT_ID order by Quantity desc) +
LAG(Quantity,1,0) over (PARTITION by PRODUCT_ID order by Quantity desc) +
LAG(Quantity,2,0) over (PARTITION by PRODUCT_ID order by Quantity desc)
as CAL_SALES
FROM SALES;
GO
/* ====================================== output =======================================
PRODUCT_ID  Quantity    CAL_SALES
----------- ----------- -----------
100         25          25
100         16          41
100         12          53
100         10          38
100         8           30
200         20          20
200         15          35
200         14          49
200         13          42
200         10          37
300         20          20
300         20          40
300         19          59
300         18          57
300         17          54
===========================================================================*/


/*----------------------------------------------------
5. Write a SQL query to find the Median of sales of a product?
------------------------------------------------*/

SELECT PRODUCT_ID, Quantity,
MEDIAN = PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY Quantity) OVER (PARTITION BY PRODUCT_ID)
FROM SALES;
GO
/* ====================================== output =======================================
PRODUCT_ID  Quantity    MEDIAN
----------- ----------- ----------------------
100         8           12
100         10          12
100         12          12
100         16          12
100         25          12
200         10          14
200         13          14
200         14          14
200         15          14
200         20          14
300         17          19
300         18          19
300         19          19
300         20          19
300         20          19
=============================================================================*/

/*----------------------------------------------------
6. Write a SQL query to find the minimum sales of a product without using the group by clause.
------------------------------------------------*/

SELECT PRODUCT_ID, YEAR, QUANTITY from
	(SELECT PRODUCT_ID, YEAR,
	 MIN(Quantity) OVER (PARTiTION BY PRODUCT_ID) as QUANTITY,
	 ROW_NUMBER() over (PARTiTION BY PRODUCT_ID order by Quantity ) as RNO
	 FROM SALES) as t
WHERE RNO = 1;
Go 

/* ====================================== output =======================================
PRODUCT_ID  YEAR        QUANTITY
----------- ----------- -----------
100         2012        8
200         2010        10
300         2008        17

=====================================================================*/
