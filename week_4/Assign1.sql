/*============================================================
Create different triggers on DB objects.
=============================================================*/

set nocount on
USE sidd;

IF OBJECT_ID('Sales','U') IS NOT NULL
DROP TABLE  Sales;

IF OBJECT_ID('MovieSales','U') IS NOT NULL
DROP TABLE  MovieSales;

IF OBJECT_ID('Record','U') IS NOT NULL
DROP TABLE  Record;

CREATE TABLE  MovieSales
(
	[MvID] INT Primary Key Identity(1,1),
	[MvName] NVARCHAR(100),
	[MvQnt] INT
);

CREATE TABLE  Sales(
	[Sid] int identity(1,1),
	[Name] VARCHAR(20),
	[City] VARCHAR(20),
	[Age] INT,
	[MvID] INT foreign key references  MovieSales(MvId)
);

Create Table  Record(
	[RcId] int identity(1,1),
	[RcText] TEXT
)

INSERT INTO  MovieSales VALUES
	('Harry Pottor and The half blood Prince',2),
	('Pirates of Carrabian: At the edge of world',1),
	('Avengers: Age of Ultron',2);
Go

---- trigger to increase or decrease count
---- trigger to add record of Sales and book issue in record table

CREATE TRIGGER decreaseCount ON  Sales 
After Insert As
BEGIN
	Declare @MvId as INT;
	SELECT @MvId = MvId from INSERTED;
	Update  MovieSales
	set MvQnt = MvQnt - 1
	From  MovieSales
	Where MvID = @MvId;

	Declare @msg NVARCHAR(100) = (SELECT 'Movie: '+ MvName + ' issued' from MovieSales where MvID = @MvId);
	PRINT @msg
END
Go

CREATE TRIGGER increaseCount ON  Sales 
After DELETE As
BEGIN
	Declare @MvId as INT;
	SELECT @MvId = MvId from deleted;
	Update  MovieSales
	set MvQnt = MvQnt + 1
	From  MovieSales
	Where MvID = @MvId;

	Declare @msg NVARCHAR(100) = (SELECT 'Movie: '+ MvName + ' Submitted' from MovieSales where MvID = @MvId);
	PRINT @msg
END
Go

CREATE TRIGGER addRecord ON  Sales 
FOR Insert, Delete As
BEGIN
	Declare @MvId as INT, @MovieName as NVARCHAR(100), @Buyer as VARCHAR(20);
	
	IF EXISTS ( SELECT 0 FROM inserted )
	BEGIN
		SELECT @MvId = MvId from INSERTED;
		SELECT @Buyer = Name from inserted;

		SET @MovieName = (SELECT MvName from MovieSales where MvID = @MvId)
	
		Insert into Record Values(CONCAT(@Buyer,N' issued the book: ',@MovieName))
	END
	IF EXISTS ( SELECT 0 FROM deleted )
	BEGIN
		SELECT @MvId = MvId from deleted;
		SELECT @Buyer = Name from deleted;

		SET @MovieName = (SELECT MvName from MovieSales where MvID = @MvId)
	
		Insert into Record Values(CONCAT(@Buyer,N' submitted the book: ',@MovieName))
	END

END
Go
----Select * from  Sales;
----GO

Select * from  MovieSales;
GO

INSERT INTO  Sales VALUES ('Avish','Banglore',24,1);
INSERT INTO  Sales VALUES ('Siddhesh','Jaipur',22,3);
INSERT INTO  Sales VALUES ('Siddhesh','Jaipur',22,1);
INSERT INTO  Sales VALUES ('Rajveer','Delhi',25,2);
Go

Select * from  MovieSales;
GO

Delete from sales where Name = 'Siddhesh' and MvID = (select Mvid from MovieSales where MvName = 'Avengers: Age of Ultron');
Delete from sales where Name = 'Avish' and MvID = (select Mvid from MovieSales where MvName = 'Harry Pottor and The half blood Prince');
Go

select * from MovieSales;
Go

Select * from Record;
Go
set nocount off


/*--------------------------------------------------------------------------------------------------
MvID        MvName                                                                                               MvQnt
----------- ---------------------------------------------------------------------------------------------------- -----------
1           Harry Pottor and The half blood Prince                                                               2
2           Pirates of Carrabian: At the edge of world                                                           1
3           Avengers: Age of Ultron                                                                              2

Movie: Harry Pottor and The half blood Prince issued
Movie: Avengers: Age of Ultron issued
Movie: Harry Pottor and The half blood Prince issued
Movie: Pirates of Carrabian: At the edge of world issued
MvID        MvName                                                                                               MvQnt
----------- ---------------------------------------------------------------------------------------------------- -----------
1           Harry Pottor and The half blood Prince                                                               0
2           Pirates of Carrabian: At the edge of world                                                           0
3           Avengers: Age of Ultron                                                                              1

Movie: Avengers: Age of Ultron Submitted
Movie: Harry Pottor and The half blood Prince Submitted
MvID        MvName                                                                                               MvQnt
----------- ---------------------------------------------------------------------------------------------------- -----------
1           Harry Pottor and The half blood Prince                                                               1
2           Pirates of Carrabian: At the edge of world                                                           0
3           Avengers: Age of Ultron                                                                              2

RcId        RcText
----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1           Avish issued the book: Harry Pottor and The half blood Prince
2           Siddhesh issued the book: Avengers: Age of Ultron
3           Siddhesh issued the book: Harry Pottor and The half blood Prince
4           Rajveer issued the book: Pirates of Carrabian: At the edge of world
5           Siddhesh submitted the book: Avengers: Age of Ultron
6           Avish submitted the book: Harry Pottor and The half blood Prince


Completion time: 2021-07-18T20:07:10.4024510+05:30
-----------------------------------------------------------------------------------------------------------------------------------*/