-------------------------- Use of Lag and Lead function ---------------------

USE tempdb;
GO

USE sidd;
GO

IF OBJECT_ID('ProductSales','U') IS NOT NULL
DROP TABLE  ProductSales;

CREATE TABLE  ProductSales
(
  [Year]	INT
 ,[Quarter] TINYINT
 ,Sales     DECIMAL(9,2)
);

INSERT INTO  ProductSales VALUES 
 (2017, 1, 35000.00)
,(2017, 2, 23000.00)
,(2017, 3, 56000.00)
,(2017, 4, 12500.00)
,(2018, 1, 11000.00)
,(2018, 2,  9400.00)
,(2018, 3, 84200.00)
,(2018, 4,  1122.00)
,(2019, 1, 55520.00)
,(2019, 2, 64000.00)
,(2019, 3, 62030.00)
,(2019, 4, 52520.00)
Go

------------------------------------------------Example 1 ------------------------------------------
--Lead Function to Fetch Next Sales Value 
SELECT [Year], [Quarter], Sales, 
       LEAD(Sales, 1, 0) OVER (ORDER BY [Quarter] ASC) as [LEAD-Next-Quarter-Sales]	  
FROM    ProductSales;
GO
------------------------------------------------Example 2 -------------------------------------------

--LAG Function to Fetch Previous Sales Value 
SELECT [Year], [Quarter], Sales
       ,LAG(Sales, 1, 0)  OVER (ORDER BY [Quarter] ASC) as [LAG-Previous-Quarter-Sales]
FROM    ProductSales;
GO

------------------------------------------------Example3 --------------------


--Lead Function With Partition By to Fetch Next Sales Value 
SELECT [Year], [Quarter], Sales, 
       LEAD(Sales, 1, 0) OVER (PARTITION BY [Year] ORDER BY [Year], [Quarter] ASC) as [LEAD-Next-Quarter-Sales]	  
FROM    ProductSales;
GO


/* ----------------------------------- OUTPUT --------------------------

(12 rows affected)
Year        Quarter Sales                                   LEAD-Next-Quarter-Sales
----------- ------- --------------------------------------- ---------------------------------------
2017        1       35000.00                                11000.00
2018        1       11000.00                                55520.00
2019        1       55520.00                                64000.00
2019        2       64000.00                                9400.00
2018        2       9400.00                                 23000.00
2017        2       23000.00                                56000.00
2017        3       56000.00                                84200.00
2018        3       84200.00                                62030.00
2019        3       62030.00                                52520.00
2019        4       52520.00                                1122.00
2018        4       1122.00                                 12500.00
2017        4       12500.00                                0.00

(12 rows affected)

Year        Quarter Sales                                   LAG-Previous-Quarter-Sales
----------- ------- --------------------------------------- ---------------------------------------
2017        1       35000.00                                0.00
2018        1       11000.00                                35000.00
2019        1       55520.00                                11000.00
2019        2       64000.00                                55520.00
2018        2       9400.00                                 64000.00
2017        2       23000.00                                9400.00
2017        3       56000.00                                23000.00
2018        3       84200.00                                56000.00
2019        3       62030.00                                84200.00
2019        4       52520.00                                62030.00
2018        4       1122.00                                 52520.00
2017        4       12500.00                                1122.00

(12 rows affected)

Year        Quarter Sales                                   LEAD-Next-Quarter-Sales
----------- ------- --------------------------------------- ---------------------------------------
2017        1       35000.00                                23000.00
2017        2       23000.00                                56000.00
2017        3       56000.00                                12500.00
2017        4       12500.00                                0.00
2018        1       11000.00                                9400.00
2018        2       9400.00                                 84200.00
2018        3       84200.00                                1122.00
2018        4       1122.00                                 0.00
2019        1       55520.00                                64000.00
2019        2       64000.00                                62030.00
2019        3       62030.00                                52520.00
2019        4       52520.00                                0.00

(12 rows affected)

---*/