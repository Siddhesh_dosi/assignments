  -------------------------------- Implement transactions in SP ----------------------------- 
  Use sidd;

create table Accounts(Aid int primary key,Amount int, type varchar(10));
create table PersonalDetails (Name Varchar(20), Age int check(Age > 18), Acc int FOREIGN KEY references Accounts(Aid) );
Go

CREATE PROCEDURE InsertDetails
       @Name varchar(20),
       @Age int,
	   @Amount int,
	   @Aid int,
	   @type varchar(10)

AS
BEGIN
       BEGIN TRAN
              BEGIN TRY
					Insert into Accounts Values
							(@Aid,@Amount, @type);
					Insert INTO PersonalDetails  VALUES
                            (@Name, @Age, @Aid);
              
              -- if not error, commit the transcation
              COMMIT TRANSACTION
       END TRY
       BEGIN CATCH
              ROLLBACK TRANSACTION
       END CATCH
END
Go

Exec InsertDetails @Name = 'Siddhesh', @Age = 21, @Amount = 2000, @Aid = 2321, @type = 'Saving';
Exec InsertDetails @Name = 'Ritesh', @Age = 17, @Amount = 3000, @Aid = 2761, @type = 'Saving';
Go

Select * from Accounts;
Go
Select * from PersonalDetails;
GO

/*-------------------------------------- output -----------------------------------------------------
Msg 2714, Level 16, State 6, Line 3
There is already an object named 'Accounts' in the database.
Msg 2714, Level 16, State 3, Procedure InsertDetails, Line 2 [Batch Start Line 5]
There is already an object named 'InsertDetails' in the database.

(0 rows affected)

(1 row affected)

(0 rows affected)
Aid         Amount      type
----------- ----------- ----------
2321        2000        Saving

(1 row affected)

Name                 Age         Acc
-------------------- ----------- -----------
Siddhesh             21          2321

(1 row affected)
--------------------------------------------*/