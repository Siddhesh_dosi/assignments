/* ------------------------ Use of Offset Fetch -------------------*/

use sidd;

IF OBJECT_ID('Record','U') IS NOT NULL
DROP TABLE  Record;

CREATE TABLE  Record
(
	ID INT Primary Key Identity(1,1),
	MvName NVARCHAR(100)
);

INSERT INTO Record Values
	('Siddhesh'),
	('Rohit'),
	('Rahul'),
	('Sudesh'),
	('Ujwal'),
	('Suraj'),
	('Rishabh'),
	('Puneet'),
	('Nitin'),
	('Purvesh'),
	('hemant'),
	('Palak'),
	('Surveer'),
	('Pranjal');
Go

Select * from Record order by MvName Desc;
Go

SELECT
    ID,MvName
FROM
    Record
ORDER BY
    MvName DESC
OFFSET 2 ROWS 
FETCH FIRST 4 ROWS ONLY;
GO


/* ========================================== OUTPUT ================================================

(14 rows affected)
ID          MvName
----------- ----------------------------------------------------------------------------------------------------
5           Ujwal
13          Surveer
6           Suraj
4           Sudesh
1           Siddhesh
2           Rohit
7           Rishabh
3           Rahul
10          Purvesh
8           Puneet
14          Pranjal
12          Palak
9           Nitin
11          hemant

(14 rows affected)

ID          MvName
----------- ----------------------------------------------------------------------------------------------------
6           Suraj
4           Sudesh
1           Siddhesh
2           Rohit

(4 rows affected)


==============================================================*/