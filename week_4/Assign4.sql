------------------------------------- Creation of dynamic queries
use sidd;

IF OBJECT_ID('Record','U') IS NOT NULL
DROP TABLE  Record;

CREATE TABLE  Record
(
	ID INT Primary Key Identity(1,1),
	MvName NVARCHAR(100)
);

INSERT INTO Record Values
	('Siddhesh'),
	('Rohit'),
	('Rahul'),
	('Sudesh'),
	('Ujwal'),
	('Suraj'),
	('Rishabh'),
	('Puneet'),
	('Nitin'),
	('Purvesh'),
	('hemant'),
	('Palak'),
	('Surveer'),
	('Pranjal');
Go

CREATE or ALTER PROC query_from_table ( @table NVARCHAR(128) )
AS BEGIN

    DECLARE @sql NVARCHAR(MAX);

    SET @sql = N'SELECT * FROM ' + @table;

    EXEC sp_executesql @sql;
    
END;
Go 

Exec query_from_table @table = 'Record';
Exec query_from_table @table = 'MovieSales';
Exec query_from_table @table = 'table5';
/*============================================ output =======================================

(14 rows affected)
ID          MvName
----------- ----------------------------------------------------------------------------------------------------
1           Siddhesh
2           Rohit
3           Rahul
4           Sudesh
5           Ujwal
6           Suraj
7           Rishabh
8           Puneet
9           Nitin
10          Purvesh
11          hemant
12          Palak
13          Surveer
14          Pranjal

(14 rows affected)

MvID        MvName                                                                                               MvQnt
----------- ---------------------------------------------------------------------------------------------------- -----------
1           Harry Pottor and The half blood Prince                                                               1
2           Pirates of Carrabian: At the edge of world                                                           0
3           Avengers: Age of Ultron                                                                              2

(3 rows affected)

id          gender
----------- ----------
1           Malea
2           Female
3           Female
4           Female
5           Male
6           Male
7           Female

(7 rows affected)
*/
