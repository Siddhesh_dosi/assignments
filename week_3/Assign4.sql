--Create a SP which will retun student details when i pass 'StudentDetails' and Product details when i pass 'ProductDetails'

use sidd;

create table Student(sid int identity(1,1), sname varchar(20), sclass int);
create table Product(pid int identity(1,1), pname varchar(20), qnt int);

insert into Student values
	('SA',5),
	('SA',4),
	('SA',8),
	('SA',5),
	('SA',5),
	('SA',8),
	('SA',6),
	('SA',9);

insert into Product values
	('Pen',2),
	('Rubber',2),
	('Scale',4),
	('Eraser',1),
	('Pencil',4),
	('Colors',2),
	('Divider',3),
	('Box',6);
GO
--------------------------------------------------------------------


Create proc details(@key as varchar(20))
As
Begin
	if @key = 'StudentDetails'
	Begin
		select * from Student;
	End
	if @key = 'ProductDetails'
	Begin
		Select * from Product;
    End
End
Go

Exec details @key = 'StudentDetails';
Go
Exec details @key = 'ProductDetails';
Go
Exec details @key = '';
Go
----------------------------------------------------------------------

Drop table Student;
Drop table Product;
Drop proc details;

/*================================output
sid         sname                sclass
----------- -------------------- -----------
1           SA                   5
2           SA                   4
3           SA                   8
4           SA                   5
5           SA                   5
6           SA                   8
7           SA                   6
8           SA                   9


pid         pname                qnt
----------- -------------------- -----------
1           Pen                  2
2           Rubber               2
3           Scale                4
4           Eraser               1
5           Pencil               4
6           Colors               2
7           Divider              3
8           Box                  6


==============================*/
