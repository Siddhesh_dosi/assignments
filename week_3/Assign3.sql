-- Using apply operator in function

use sidd;

create table department(did int primary key identity(1,1), dname varchar(20));

create table employee(eid int identity(100,1), ename varchar(30), did int references department(did));

insert into department values
	('Home'),
	('Admin'),
	('IT'),
	('HR'),
	('Sales');

insert into employee values
	('A',1),
	('B',2),
	('C',3),
	('D',2),
	('E',3),
	('F',3),
	('G',1),
	('H',1),
	('I',1),
	('J',2),
	('L',2),
	('M',3);

Go

CREATE FUNCTION GetAllEmployeeOfDepartment (@DeptID AS INT)
	RETURNS TABLE
	AS
	RETURN (
			SELECT E.eid, E.ename
			FROM Employee E
			WHERE E.did = @DeptID
			)
GO

create proc getEmp
as	
	SELECT *
	FROM Department
	CROSS APPLY GetAllEmployeeOfDepartment(Department.did)
	order by department.did;
Go

Exec getEmp;

------------------------------------------
Drop function GetAllEmployeeOfDepartment;
Drop proc getEmp;
Drop table employee;
Drop table department;




/* ================== output ==========================
did         dname                eid         ename
----------- -------------------- ----------- ------------------------------
1           Home                 100         A
1           Home                 106         G
1           Home                 107         H
1           Home                 108         I
2           Admin                109         J
2           Admin                110         L
2           Admin                101         B
2           Admin                103         D
3           IT                   104         E
3           IT                   105         F
3           IT                   102         C
3           IT                   111         M

=========================================================*/
