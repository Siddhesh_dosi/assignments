-- 1. Use of default argument in stored procedure

use sidd;

CREATE TABLE pivot_demo    
(    
   Region varchar(45),    
   Year int,    
   Sales int    
)

INSERT INTO pivot_demo
VALUES ('North', 2010, 72500),  
('South', 2010, 60500),  
('South', 2010, 52000),  
('North', 2011, 45000),  
('South', 2011, 82500),    
('North', 2011, 35600),  
('South', 2012, 32500),   
('North', 2010, 20500);   
Go


create proc createPivot(@pivotCol as varchar(10) = 'YEAR')
as 
IF @pivotCol = 'REGION'
	BEGIN
		SELECT Year, North, South 
			FROM (SELECT Region, Year, Sales FROM pivot_demo ) AS Tab1    
		PIVOT    
			(SUM(Sales) FOR Region IN (North, South)) AS Tab2    
		ORDER BY Tab2.Year;
		Return
	END
ELSE
	Begin
		SELECT Region, [2010], [2011], [2012] FROM     
		(SELECT Region, [Year], Sales FROM pivot_demo ) AS Tab1    
		PIVOT    
		(SUM(Sales) FOR [Year] IN ([2010], [2011], [2012])) AS Tab2  
		ORDER BY Tab2.Region;   
	End;
Go
-------------------------------------------------------------------------
Exec createPivot;
GO

Exec createPivot @pivotCol = 'REGION'
GO

DROP PROCEDURE createPivot;
Drop table pivot_demo;




/*-------------------------------------Output----------------------------------------------

Region                                        2010        2011        2012
--------------------------------------------- ----------- ----------- -----------
North                                         93000       80600       NULL
South                                         112500      82500       32500

==========================================================================================
Year        North       South
----------- ----------- -----------
2010        93000       112500
2011        80600       82500
2012        NULL        32500

----*/


