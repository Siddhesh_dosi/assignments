use sidd;

create table sales (id int identity(1,1), salesman_id int, sales_num int);

insert into sales values
(1,14),
(2,20),
(3,25),
(2,33),
(3,12),
(1,15),
(4,4),
(4,56),
(4,24),
(4,12),
(5,9),
(1,1),
(2,33),
(2,17),
(2,18),
(3,5),
(3,4),
(1,2);
GO


SELECT id,
  salesman_id,
  sales_num,
  rank() over (partition by salesman_id order by sales_num ) as RANK
FROM sales
GO


Drop table sales;

/*================================output
id          salesman_id sales_num   RANK
----------- ----------- ----------- --------------------
12          1           1           1
18          1           2           2
1           1           14          3
6           1           15          4
14          2           17          1
15          2           18          2
2           2           20          3
4           2           33          4
13          2           33          4
17          3           4           1
16          3           5           2
5           3           12          3
3           3           25          4
7           4           4           1
10          4           12          2
9           4           24          3
8           4           56          4
11          5           9           1
======================================================8/