-- Implement nested stored procedure


create table employee(
	eid int primary key identity(1,1),
	fname varchar(20),
	lname varchar(20),
	);

insert into employee values
	('Sid','Dosi'),
	('Himanshu','Dadhich'),
	('Ram','mehta'),
	('Hannah','david'),
	('Ronnie','david'),
	('Sid','panabacker'),
	('John','david');
Go

create proc selectFname(@value as varchar(20))
as 
	select * from employee where fname = @value;
go

Create proc selectLname(@value as  varchar(20))
as 
	select * from employee where lname = @value;
Go

create proc selectEmp(@key as varchar(20) = '', @value as varchar(20))
as
Begin
	IF @key = 'first'
		exec selectFname @value = @value;
	Else IF @key = 'last'
		exec selectLname @value = @value;
End;
Go


Exec selectEmp @key = 'first', @value = 'sid';
go

Exec selectEmp @key = 'last', @value = 'david';
go


drop table employee;
drop proc selectFname;
drop proc selectLname;
drop proc selectEmp;


/*----------------- output-----------------------------
eid         fname                lname
----------- -------------------- --------------------
1           Sid                  Dosi
6           Sid                  panabacker

(2 rows affected)

eid         fname                lname
----------- -------------------- --------------------
4           Hannah               david
5           Ronnie               david
7           John                 david
------------------------------------------------------*/