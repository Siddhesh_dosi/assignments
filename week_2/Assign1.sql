-- 1>.Insert into Temp tables and Table Variables  

Create table #Course (Cid Smallint identity(1,1) Primary key, Cname Varchar(20) not null, cInstructor varchar(20)) ;
insert into #Course values('python','Amit');
insert into #Course values('SQL','Suresh');  
Select @@Identity 
-- 2
Delete from #Course where Cname = 'SQL';
insert into #Course values('SQL','Suresh');  
Select @@Identity
-- 3

select SCOPE_IDENTITY();
-- 3

-----------------------------------------------------------------------
-- Temp variable

DECLARE @Course TABLE (Cid Smallint identity(1,1) Primary key, Cname Varchar(20) not null, cInstructor varchar(20));
insert into @Course values('python','Amit');
insert into @Course values('SQL','Suresh');  
Select @@Identity 
-- 2
Delete from @Course where Cname = 'SQL';
insert into @Course values('SQL','Suresh');  
Select @@Identity
-- 3

select SCOPE_IDENTITY();

---------------------------------------------------------------------------------------------

