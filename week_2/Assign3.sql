-- 3>. 
-- use of different constriants in a table and their voilations

Use sidd;

create table otherName(
	did int Primary key,
	dname varchar(50)
	);

create table ConstraintVoilation(
	pk int PRIMARY KEY,
	name varchar(50) not null,
	fk int FOREIGN KEY REFERENCES otherName(did),
	age int check (age>=18),
	title varchar(50) default 'intern'
	);

insert into otherName Values(1,'IT');
insert into otherName Values(2,'CS');
insert into otherName Values(3,'MECH');

begin try
	insert into ConstraintVoilation(pk,name,fk,age,title) Values(1,'suresh',1,32,'jr. developer');
End try
begin catch
	select 
		ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
end catch
begin try
	insert into ConstraintVoilation(pk,name,fk,age) Values(2,'Ramesh',1,20);                   -- default
	select * from ConstraintVoilation;
End try
begin catch
	select 
		ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
end catch
begin try
	insert into ConstraintVoilation(pk,name,fk,age) Values(3,'Hitesh',1,17);                   -- check
	select * from ConstraintVoilation;

End try
begin catch
	select 
		ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
end catch
begin try
	insert into ConstraintVoilation(pk,name,fk,age,title) Values(1,'Rajesh',2,22,'jr. developer');   -- pk
	select * from ConstraintVoilation;

End try
begin catch
	select 
		ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
end catch
begin try
	insert into ConstraintVoilation(pk,name,fk,age,title) Values(4,'Mahesh',4,32,'jr. developer');   -- fk
select * from ConstraintVoilation;
End try
begin catch
	select 
		ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
end catch
begin try
	insert into ConstraintVoilation(pk,name,fk,age,title) Values(5,Null,3,32,'jr. developer');       -- not null
	select * from ConstraintVoilation;

End try
begin catch
	select 
		ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
end catch

select * from otherName;
select * from ConstraintVoilation;


--declare @id int;





---- output ---
/*
(0 rows affected)
ErrorNumber ErrorLine   ErrorMessage
----------- ----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2627        21          Violation of PRIMARY KEY constraint 'PK__Constrai__321403CF4C5553E6'. Cannot insert duplicate key in object 'dbo.ConstraintVoilation'. The duplicate key value is (1).

(1 row affected)

(0 rows affected)

ErrorNumber ErrorLine   ErrorMessage
----------- ----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2627        30          Violation of PRIMARY KEY constraint 'PK__Constrai__321403CF4C5553E6'. Cannot insert duplicate key in object 'dbo.ConstraintVoilation'. The duplicate key value is (2).

(1 row affected)

(0 rows affected)

ErrorNumber ErrorLine   ErrorMessage
----------- ----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
547         40          The INSERT statement conflicted with the CHECK constraint "CK__ConstraintV__age__2A4B4B5E". The conflict occurred in database "sidd", table "dbo.ConstraintVoilation", column 'age'.

(1 row affected)

(0 rows affected)

ErrorNumber ErrorLine   ErrorMessage
----------- ----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2627        51          Violation of PRIMARY KEY constraint 'PK__Constrai__321403CF4C5553E6'. Cannot insert duplicate key in object 'dbo.ConstraintVoilation'. The duplicate key value is (1).

(1 row affected)

(0 rows affected)

ErrorNumber ErrorLine   ErrorMessage
----------- ----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
547         62          The INSERT statement conflicted with the FOREIGN KEY constraint "FK__ConstraintVo__fk__29572725". The conflict occurred in database "sidd", table "dbo.otherName", column 'did'.

(1 row affected)

(0 rows affected)

ErrorNumber ErrorLine   ErrorMessage
----------- ----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
515         72          Cannot insert the value NULL into column 'name', table 'sidd.dbo.ConstraintVoilation'; column does not allow nulls. INSERT fails.

(1 row affected)

did         dname
----------- --------------------------------------------------
1           IT
2           CS
3           MECH

(3 rows affected)

pk          name                                               fk          age         title
----------- -------------------------------------------------- ----------- ----------- --------------------------------------------------
1           suresh                                             1           32          jr. developer
2           Ramesh                                             1           20          intern

(2 rows affected)


*/

-- ===========================================================================================================================
