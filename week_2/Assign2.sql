
-- 2>. Implement Try-Catch using Transactions and get Error Number/ Message/ Severity



CREATE TABLE persons
(
   person_id  INT PRIMARY KEY, 
   first_name NVARCHAR(100) NOT NULL, 
   last_name  NVARCHAR(100) NOT NULL
);


BEGIN TRY
	BEGIN TRANSACTION;
		insert into 
		   persons(person_id,first_name, last_name)
		values
			(1,'John','Doe'),
			(1,'Jane','Doe');
	COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	SELECT   
        ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_MESSAGE() AS ErrorMessage;  
END CATCH

----- output
-- ErrorNumber ErrorSeverity ErrorState  ErrorLine   ErrorProcedure         ErrorMessage
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- 2627        14            1           40          NULL          Violation of PRIMARY KEY constraint 'PK__persons__543848DF019BBDE4'. Cannot insert duplicate key in object 'dbo.persons'. The duplicate key value is (1).

----------------------------------------------------------
----------------------------------------------------------

