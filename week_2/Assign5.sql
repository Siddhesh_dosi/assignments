
-- 5>.  Differentiate Sub-query, Co- related Subquery and Derived tables with examples.

Correlated Query is nothing but the subquery whose output is depending on the inner query used in that query
“Correlated Queries are also called as Synchronized queries…”
Example
Select * from Employee E where Not exist
(Select Department_no From Department D where E.Employee_id=D.Employee_ID);

Execution of query:

Step 1:

Select * from Employee E ;

It will fetch the all employees

Step 2:

The First Record of the Employee second query is executed and output is given to first query.

(Select Department_no From Department D where E.Employee_id=D.Employee_ID);

Step 3:

Step 2 is repeated until and unless all output is been fetched.
------------------------------------------------------------------------------------------------

- derived tables are used in the FROM clause​
- subqueries are used in the WHERE clause, but can also ​be used to select from one table and insert into ​another

# Subquery example:

select employee_name ​
from employee​
where employee_salary >​
-- this is a subquery:​
(select avg(employee_salary)​
        from employee)
----------------------------------------------------
# Derived tables example

select max(age) ​
from (​​
select age from table​
) as Age -- must give derived table an alias​

-----------------------------------------------------

