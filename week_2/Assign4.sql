
-- ===========================================================================================================================
-- 4>. Find productID and a column which will show product_sale as Good  if the total price of sales of that product is > 2500 else it will show Bad

use company;

create table product_sale(
	TxnNo int Primary Key identity(1,1),
	PId int,
	Price float
	);

insert into product_sale(PId, Price) values(1,750),(1,800),(2,1000),(1,1000),(2,1000);

select PId, sum(Price) TotalSales,
case 
	when sum(Price)> 2500 then 'Yes'
	when sum(Price)<= 2500 then 'No'
End as CouponEligible
from product_sale
group by PId;

-- ++++++=+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
