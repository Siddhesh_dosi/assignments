-- 1>.Insert into Temp tables and Table Variables  

Create table #Course (Cid Smallint identity(1,1) Primary key, Cname Varchar(20) not null, cInstructor varchar(20)) ;
insert into #Course values('python','Amit');
insert into #Course values('SQL','Suresh');  
Select @@Identity 
-- 2
Delete from #Course where Cname = 'SQL';
insert into #Course values('SQL','Suresh');  
Select @@Identity
-- 3

select SCOPE_IDENTITY();
-- 3

-----------------------------------------------------------------------
-- Temp variable

DECLARE @Course TABLE (Cid Smallint identity(1,1) Primary key, Cname Varchar(20) not null, cInstructor varchar(20));
insert into @Course values('python','Amit');
insert into @Course values('SQL','Suresh');  
Select @@Identity 
-- 2
Delete from @Course where Cname = 'SQL';
insert into @Course values('SQL','Suresh');  
Select @@Identity
-- 3

select SCOPE_IDENTITY();

---------------------------------------------------------------------------------------------


-- 2>. Implement Try-Catch using Transactions and get Error Number/ Message/ Severity



CREATE TABLE persons
(
   person_id  INT PRIMARY KEY, 
   first_name NVARCHAR(100) NOT NULL, 
   last_name  NVARCHAR(100) NOT NULL
);


BEGIN TRY
	BEGIN TRANSACTION;
		insert into 
		   persons(person_id,first_name, last_name)
		values
			(1,'John','Doe'),
			(1,'Jane','Doe');
	COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	SELECT   
        ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_MESSAGE() AS ErrorMessage;  
END CATCH

----- output
-- ErrorNumber ErrorSeverity ErrorState  ErrorLine   ErrorProcedure         ErrorMessage
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- 2627        14            1           40          NULL          Violation of PRIMARY KEY constraint 'PK__persons__543848DF019BBDE4'. Cannot insert duplicate key in object 'dbo.persons'. The duplicate key value is (1).

----------------------------------------------------------
----------------------------------------------------------

-- 3>. 
Use sidd;

create table otherName(
	did int Primary key,
	dname varchar(50)
	);

create table ConstraintVoilation(
	pk int PRIMARY KEY,
	name varchar(50) not null,
	fk int FOREIGN KEY REFERENCES otherName(did),
	age int check (age>=18),
	title varchar(50) default 'intern'
	);

insert into otherName Values(1,'IT');
insert into otherName Values(2,'CS');
insert into otherName Values(3,'MECH');

begin try
	insert into ConstraintVoilation(pk,name,fk,age,title) Values(1,'suresh',1,32,'jr. developer');
End try
begin catch
	select 
		ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
end catch
begin try
	insert into ConstraintVoilation(pk,name,fk,age) Values(2,'Ramesh',1,20);                   -- default
	select * from ConstraintVoilation;
End try
begin catch
	select 
		ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
end catch
begin try
	insert into ConstraintVoilation(pk,name,fk,age) Values(3,'Hitesh',1,17);                   -- check
	select * from ConstraintVoilation;

End try
begin catch
	select 
		ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
end catch
begin try
	insert into ConstraintVoilation(pk,name,fk,age,title) Values(1,'Rajesh',2,22,'jr. developer');   -- pk
	select * from ConstraintVoilation;

End try
begin catch
	select 
		ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
end catch
begin try
	insert into ConstraintVoilation(pk,name,fk,age,title) Values(4,'Mahesh',4,32,'jr. developer');   -- fk
select * from ConstraintVoilation;
End try
begin catch
	select 
		ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
end catch
begin try
	insert into ConstraintVoilation(pk,name,fk,age,title) Values(5,Null,3,32,'jr. developer');       -- not null
	select * from ConstraintVoilation;

End try
begin catch
	select 
		ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_LINE () AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
end catch

select * from otherName;
select * from ConstraintVoilation;


--declare @id int;





---- output ---
/*
(0 rows affected)
ErrorNumber ErrorLine   ErrorMessage
----------- ----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2627        21          Violation of PRIMARY KEY constraint 'PK__Constrai__321403CF4C5553E6'. Cannot insert duplicate key in object 'dbo.ConstraintVoilation'. The duplicate key value is (1).

(1 row affected)

(0 rows affected)

ErrorNumber ErrorLine   ErrorMessage
----------- ----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2627        30          Violation of PRIMARY KEY constraint 'PK__Constrai__321403CF4C5553E6'. Cannot insert duplicate key in object 'dbo.ConstraintVoilation'. The duplicate key value is (2).

(1 row affected)

(0 rows affected)

ErrorNumber ErrorLine   ErrorMessage
----------- ----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
547         40          The INSERT statement conflicted with the CHECK constraint "CK__ConstraintV__age__2A4B4B5E". The conflict occurred in database "sidd", table "dbo.ConstraintVoilation", column 'age'.

(1 row affected)

(0 rows affected)

ErrorNumber ErrorLine   ErrorMessage
----------- ----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2627        51          Violation of PRIMARY KEY constraint 'PK__Constrai__321403CF4C5553E6'. Cannot insert duplicate key in object 'dbo.ConstraintVoilation'. The duplicate key value is (1).

(1 row affected)

(0 rows affected)

ErrorNumber ErrorLine   ErrorMessage
----------- ----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
547         62          The INSERT statement conflicted with the FOREIGN KEY constraint "FK__ConstraintVo__fk__29572725". The conflict occurred in database "sidd", table "dbo.otherName", column 'did'.

(1 row affected)

(0 rows affected)

ErrorNumber ErrorLine   ErrorMessage
----------- ----------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
515         72          Cannot insert the value NULL into column 'name', table 'sidd.dbo.ConstraintVoilation'; column does not allow nulls. INSERT fails.

(1 row affected)

did         dname
----------- --------------------------------------------------
1           IT
2           CS
3           MECH

(3 rows affected)

pk          name                                               fk          age         title
----------- -------------------------------------------------- ----------- ----------- --------------------------------------------------
1           suresh                                             1           32          jr. developer
2           Ramesh                                             1           20          intern

(2 rows affected)


*/

-- ===========================================================================================================================
-- 4>. Find productID and a column which will show product_sale as Good  if the total price of sales of that product is > 2500 else it will show Bad

use company;

create table product_sale(
	TxnNo int Primary Key identity(1,1),
	PId int,
	Price float
	);

insert into product_sale(PId, Price) values(1,750),(1,800),(2,1000),(1,1000),(2,1000);

select PId, sum(Price) TotalSales,
case 
	when sum(Price)> 2500 then 'Yes'
	when sum(Price)<= 2500 then 'No'
End as CouponEligible
from product_sale
group by PId;

-- ++++++=+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- 5>.  Differentiate Sub-query, Co- related Subquery and Derived tables with examples.

Correlated Query is nothing but the subquery whose output is depending on the inner query used in that query
“Correlated Queries are also called as Synchronized queries…”
Example
Select * from Employee E where Not exist
(Select Department_no From Department D where E.Employee_id=D.Employee_ID);

Execution of query:

Step 1:

Select * from Employee E ;

It will fetch the all employees

Step 2:

The First Record of the Employee second query is executed and output is given to first query.

(Select Department_no From Department D where E.Employee_id=D.Employee_ID);

Step 3:

Step 2 is repeated until and unless all output is been fetched.
------------------------------------------------------------------------------------------------

- derived tables are used in the FROM clause​
- subqueries are used in the WHERE clause, but can also ​be used to select from one table and insert into ​another

# Subquery example:

select employee_name ​
from employee​
where employee_salary >​
-- this is a subquery:​
(select avg(employee_salary)​
        from employee)
----------------------------------------------------
# Derived tables example

select max(age) ​
from (​​
select age from table​
) as Age -- must give derived table an alias​

-----------------------------------------------------

