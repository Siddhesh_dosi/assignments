-- 1.

-- Create users and logins and assign them different roles and rectify their access from other servers.
create login myLogin with password='Windows@123';

CREATE USER Siddhesh FOR LOGIN myLogin;
-----------------------------------------------------------------------------

-- 2.
-- Null Handling - using IsNull, Coalesce etc.

SELECT SUM(ISNULL(Salary, 10000) AS Salary
FROM Employee;

SELECT Name, COALESCE(CurrentAdd, PermanentAdd) as Address 
From Employee 
Where Salary > 20000;
-------------------------------------------------------------------------------

-- 3.
-- Different date styles using Convert and Format functions.

SELECT FORMAT(26062021, '##-##-#####');
-- 26-06-2021
SELECT FORMAT(26062021, '##/##/#####');
-- 26/06/2021
SELECT FORMAT(26062021, '##.##.#####');
-- 26.06.2021

SELECT CONVERT(VARCHAR(20), GETDATE(), 1);
SELECT CONVERT(VARCHAR(20), GETDATE(), 20) ;
SELECT CONVERT(VARCHAR(20), GETDATE(), 101);

------------------------------------------------------------------------------
-- 4.
-- Apply Identity Specification and make use of Current/ Scope Identity.

-- The @@identity function returns the last identity created in the same session.
-- The scope_identity() function returns the last identity created in the same session and the same scope.
-- The ident_current(name) returns the last identity created for a specific table or view in any session.
-- The identity() function is not used to get an identity, it's used to create an identity in a select...into query.

Create table Course (Cid Smallint identity(1,1) Primary key, Cname Varchar(20) not null, cInstructor varchar(20)) ;
insert into dbo.Course values('python','Amit');
insert into dbo.Course values('SQL','Suresh');  
Select @@Identity 
-- 2
Delete from Course where Cname = 'SQL';
insert into dbo.Course values('SQL','Suresh');  
Select @@Identity
-- 3

select SCOPE_IDENTITY();
-- 3
--------------------------------------------------------------------------------

-- 5. Use of Cast and Convert Functions.

CAST('195' AS int);
 
CONVERT(int, '225');

Select CAST('100' as int) + CAST('200' as int) +CAST ('300' as int) as Result

-- Result
-- 600

select convert(varchar(20),GETDATE(),101)

-- 06/28/2021

-----------------------------------------------------------------------------

--6. Find N'th highest Salary

DECLARE @A smallint  
set @A=3
Select Top 1 Salary From
(Select DISTINCT TOP A Salary
From Employee
Order By Salary Desc)
Result
Order By Salary)